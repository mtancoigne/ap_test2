RSpec.shared_context 'with authenticated member', shared_context: :metadata do
  before do
    sign_in User.find_by(email: 'user@example.com')
  end
end
